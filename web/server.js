import express from 'express';
import amqp from 'amqplib/callback_api';

const app  = express(),
      port = 8080;

amqp.connect('amqp://rabbitmq', (err, conn) => {
  if (err) { throw err; }

  conn.createChannel((err, ch) => {
    const qIn = 'hello/response', qOut = 'hello';

    ch.assertQueue(qIn, {durable: false});
    ch.assertQueue(qOut, {durable: false});

    app.get('/', (req, res) => {
      ch.consume(qIn, (msg) => {
        console.log(`Received: ${msg.content.toString()}`);
        res.send(msg.content.toString());
      });

      ch.sendToQueue(qOut, new Buffer('Hello!'));
      console.log('Sent hello to rabbitmq');
    });

  });

  app.listen(port)
  console.log(`Magic happening @ http://localhost:${port}.`);
});
