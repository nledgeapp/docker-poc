// Transpile ESNext to es5 on the fly.
require('babel-register')({
  presets: ['es2015']
});

// Launch the actual application.
require('./server.js');
