import amqp from 'amqplib/callback_api';

amqp.connect('amqp://rabbitmq', (err, conn) => {
  if (err) { throw err; }

  conn.createChannel((err, ch) => {
    const qIn = 'hello', qOut = 'hello/response';

    ch.assertQueue(qIn, {durable: false});
    ch.assertQueue(qOut, {durable: false});

    ch.consume(qIn, (msg) => {
      console.log(`Received: ${msg.content.toString()}`);
      ch.sendToQueue(qOut, new Buffer('Hello from Provider!'));
    });
  });

  console.log('Listening...');
});
